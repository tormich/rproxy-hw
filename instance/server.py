"""
    This is some stupid instance.
    No common seance was integrated in it.
"""

import tornado.ioloop
from tornado.options import parse_command_line, define, options
import tornado.web


def do_some_work():
    j = 0
    for i in xrange(0, 1000):
        j += i % 5

    return j


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        do_some_work()
        self.write("GET request INSTANCE - {}".format(do_some_work()))

    def post(self, *args, **kwargs):
        do_some_work()
        self.write("POST request  INSTANCE - {}".format(do_some_work()))


define("port", default=8010)

application = tornado.web.Application([
    (r"/register", MainHandler),
    (r"/changePassword", MainHandler),
    (r"/login", MainHandler),
])

if __name__ == "__main__":
    parse_command_line()
    application.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()