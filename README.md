# Welcome to rproxy

## Task
devise a load balancer while ensuring the following:

1. Each POST request is forwarded to N servers.

2. GET requests are forwarded to a single server, selected in a round-robin fashion.

3. Should be able to support N<10 servers.

5. Should be able to withstand a POST request rate of 100 req/sec and GET request rate of 1000 req/sec on a reasonable machine.


## Flow
**nginx** will forward all GET requests to one of backends. And POST requests will go to RProxy.
RProxy will then send POST to every backend. "Ok" result (200 <= status_code < 400) will be immediately returned to nginx.   

## Starting
1. copy **nginx.config/exam** to your **nginx/sites-enabled** folder and restart nginx.
2. run **run_instances.sh** (it will run all 9 instances and few instances of RProxy)

## Testing
I used **siege** tool to test it.
```
siege -c 350 -b -t30s 'http://localhost/changePassword POST'
```

## My results
POST request
```
Transactions:		        3655 hits
Availability:		      	100.00 %
Elapsed time:		       	29.84 secs
Data transferred:	        0.17 MB
Response time:		        2.71 secs
Transaction rate:	      	122.49 trans/sec
Throughput:		        	0.01 MB/sec
Concurrency:		      	332.27
Successful transactions:    3655
Failed transactions:	    0
Longest transaction:	    3.66
Shortest transaction:	    0.05
```

GET request
```
Transactions:		       	52886 hits
Availability:		      	100.00 %
Elapsed time:		       	29.05 secs
Data transferred:	        2.37 MB
Response time:		        0.19 secs
Transaction rate:	     	1820.52 trans/sec
Throughput:		        	0.08 MB/sec
Concurrency:		      	346.07
Successful transactions:    52886
Failed transactions:	    0
Longest transaction:	    7.76
Shortest transaction:	    0.00

```