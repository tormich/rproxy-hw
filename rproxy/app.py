from tornado.web import asynchronous
from tornado.httpclient import AsyncHTTPClient, HTTPRequest
import tornado.ioloop
from tornado.options import parse_command_line, define, options
import tornado.web


INSTANCES = ("localhost:8010",
             "localhost:8011",
             "localhost:8012",
             "localhost:8013",
             "localhost:8014",
             "localhost:8015",
             "localhost:8016",
             "localhost:8017",
             "localhost:8018")


class MainHandler(tornado.web.RequestHandler):
    def initialize(self):
        self._instance_count = len(INSTANCES)
        self._cardinal_err_response = None

    @asynchronous
    def post(self, *args, **kwargs):
        http_client = AsyncHTTPClient()
        path = self.request.uri

        if self.request.query:
                path += "?{query}".format(query=self.request.query)

        for i in INSTANCES:
            url = "http://{host}{path}".format(host=i, path=path)
            http_request = HTTPRequest(url=url,
                                       method='POST',
                                       body=self.request.body,
                                       headers=self.request.headers)

            http_client.fetch(http_request, callback=self.on_fetch)

    def on_fetch(self, response):
        """
        on_fetch will get response from every instance.
        In case one of them is "ok" response (error code between 200 and 399)
        it will be sent downstream immediately.
        Otherwise the one with bigger error code will be sent.
        """
        self._instance_count -= 1

        if self._finished:
            return

        if 200 <= response.code < 400:
            for h in response.headers:
                self.add_header(h, response.headers.get(h))
            self.set_status(response.code, response.reason)
            self.write(response.body)
            self.finish()
            return

        elif self._cardinal_err_response is None or self._cardinal_err_response.code < response.code:
            self._cardinal_err_response = response

        if self._instance_count == 0:
            self.set_status(self._cardinal_err_response.code, response.reason)
            for h in self._cardinal_err_response.headers:
                self.add_header(h, self._cardinal_err_response.headers.get(h))

            self.write(str(self._cardinal_err_response.body))
            self.finish()


define("port", default=8080)

application = tornado.web.Application([
    (r"/(.*)", MainHandler),
])

if __name__ == "__main__":
    parse_command_line()
    application.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()